import dash_html_components as html
import dash_core_components as dcc

from components.shared.title import subtitle
from components.shared.circular_progressbar import circular_progressbar, return_ic_ratio

from .significance_box.significance_box import change_signif_callback
from .significance_box.significance_box import significance_box
from .age_quantile_histogram.age_quantile_histogram import age_quantile_histogram
from .age_quantile_histogram.age_quantile_histogram import register_callbacks as register_callbacks_hist

# Toplevel component that renders the hospital overview page (first screen at /)


class HospitalOverviewPage:
    def __init__(self, app):
        register_callbacks_hist(app)
        change_signif_callback(app)

    def render(self):
        return html.Div(className='container', children=[
            html.Div(className='tile is-ancestor', children=[
                # Significance box at the left side
                html.Div(className='tile is-4 is-parent', children=[
                    html.Div(className='tile is-child box',
                             children=[dcc.Slider(
                               id='significance-threshold-slider', min=1, max=30, value=10,
                               marks={0: '0', 5: '5', 10: '10',
                                      15: '15', 20: '20', 25: '25', 30: '30'},
                             ), significance_box()])
                ]),
                # Right side
                html.Div(className='tile is-8 is-vertical', children=[
                    # Top graph wrapper
                    html.Div(className='tile is-parent', children=[
                        html.Div(className='tile is-child box',
                                 children=age_quantile_histogram())
                    ]),
                    # The 3 circular progress bars that display the hospital wards occupancy
                    subtitle(
                        'Percentage of covid positive patients treated in hospital wards'),
                    html.Div(className='tile', children=[
                        html.Div(className='tile is-parent is-4', children=[
                            html.Div(className='tile is-child box', children=[
                             subtitle(
                                 'non-ICU ({}%)'.format(str(round(return_ic_ratio('non-icu')*100, 3)))),
                             html.Small("Click to filter"),
                             circular_progressbar(
                                 'non-icu-occ', 'Non-ICU', progress=return_ic_ratio('non-icu'), color='#47F02B')
                             ])
                        ]),
                        html.Div(className='tile is-parent is-4', children=[
                            html.Div(className='tile is-child box', children=[
                                subtitle(
                                    'Semi-ICU ({}%)'.format(str(round(return_ic_ratio('semi-icu')*100, 3)))),
                                html.Small("Click to filter"),
                                circular_progressbar(
                                    'semi-icu-occ', 'Semi-ICU', progress=return_ic_ratio('semi-icu'), color='#FFEC43')
                            ])
                        ]),
                        html.Div(className='tile is-parent is-4', children=[
                            html.Div(className='tile is-child box', children=[
                                subtitle('ICU ({}%)'.format(
                                    str(round(return_ic_ratio('icu')*100, 3)))),
                                html.Small("Click to filter"),
                                circular_progressbar(
                                    'icu-occ', 'ICU', progress=return_ic_ratio('icu'), color='#F21905')
                            ])
                        ])
                    ])
                ])
            ])
        ])
