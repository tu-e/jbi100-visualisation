import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Input, Output
import plotly.graph_objects as go

from components.pages.diagnostic_explorer.known_test_entry.predictor import return_attr_importances

feature_importances = return_attr_importances()

# Component for the significance box on the left side of the page
def significance_box():
  return html.Div(children=[
    # Title
    html.H1('Quick select distributions', className='title'),

    # Helper to display the direction of the graph
    html.Span(className='icon', children=html.I(className='fas fa-sort-down')),
    html.Small('Descending significance'),
    
    # The actual graph
    html.Div(style={'height': '80vh'}, children=[
      dcc.Graph(id='significance-figure', style={'height': '100%', 'width': '100%'})
    ])
  ])

# Connect slider to the figure to configure the amount of attributes displayed
def change_signif_callback(app):
  @app.callback(
    Output('significance-figure', 'figure'),
    Input('significance-threshold-slider', 'value'))
  def update_fgure(thresh):
    # Render figure
    fig = go.Figure(go.Bar(
      x=feature_importances['coefficient'][:thresh],
      text=feature_importances['attribute'][:thresh],
      orientation='h',
      textposition='auto',
      marker_color='#cd5c5c'
    ))

    fig.update_layout(
      yaxis={
        'showticklabels': False,
        'autorange': 'reversed',
      },
      margin = dict(t=0, l=0, r=0, b=0),
      transition_duration=500,
    )
    return fig