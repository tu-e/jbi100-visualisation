import dash
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Input, Output

import plotly.express as px
import plotly.graph_objects as go

from components.shared.title import title, subtitle

from data_processing.main_dataset import get_main_dataset

# Use the globally available dataset as a source
pd_dataset = get_main_dataset()

# Render the age_quantile histogram firgure
def age_quantile_hist_fig(selected_color):
    fig = px.histogram(
        pd_dataset,
        x='Patient age quantile',
        color_discrete_sequence=[selected_color]
    )
    fig.update_layout(
        yaxis_title_text='Frequency',
        bargap=0.2,
        margin=dict(t=0, l=0, r=0, b=0),
        clickmode='event+select'
    )
    return fig

# Render the Hematocrit distribution figure
def hema_dist_fig():
    fig = px.histogram(
        pd_dataset,
        x='Hematocrit',
        color_discrete_sequence=['indianred']
    )
    fig.update_layout(
        yaxis_title_text='Frequency',
        margin=dict(t=0, l=0, r=0, b=0)
    )
    return fig

# Render the age cirlcle connected graph
def age_circle(selected_color, ward_type='None'):
    # ward = ['Regular Ward', 'semi-intensive unit', 'Intensive Care Ward']

    age_groups = pd_dataset.groupby('Patient age quantile')[
        ['Regular Ward', 'semi-intensive unit', 'Intensive Care Ward']].sum()
    fig = go.Figure()

    if ward_type == 'Regular Ward':
        color = '#47F02B'
    elif ward_type == 'semi-intensive unit':
        color = '#FFEC43'
    else:
        color = '#F21905'

    ward_age_groups = age_groups[ward_type]
    fig.add_trace(
        go.Bar(
            x=ward_age_groups.index,
            y=ward_age_groups.values,
            marker_color=color
        )
    )
    fig.update_layout(
        showlegend=False,
        bargap=0.2,
        clickmode='event+select',
        title='Distribution of {}'.format(ward_type),
        yaxis_title_text='Number of patients in ward',
        xaxis_title_text='Patient Age Quantile',
        transition_duration=500,
    )
    return fig

# Render the quantile per hospital ward figure
def age_quantile_hosp_fig(configuration):
    fig = go.Figure()

    age_groups = pd_dataset.groupby('Patient age quantile')[
        ['Regular Ward', 'semi-intensive unit', 'Intensive Care Ward']].sum()

    level = ['normal', 'SIC', 'IC'].index(configuration)
    for i, ward in enumerate(age_groups):
        sign = 1 if i >= level else -1
        ward_age_groups = age_groups[ward]
        fig.add_trace(go.Bar(x=ward_age_groups.index,
                             y=ward_age_groups.values * sign,
                             name=ward))

    fig.update_layout(barmode='relative')
    fig.update_layout(
        yaxis_title_text='Total occupation (per quant)',
        clickmode="event+select",
        bargap=0.2,
        margin=dict(t=0, l=0, r=0, b=0),
        legend=dict(
            yanchor="top",
            y=0.99,
            xanchor="right",
            x=0.99),
        legend_title_text='Hospital Ward:'
    )
    return fig

# Helpers to conditionally show the configuration dropdowns
hidden_style = {"visibility": "hidden"}
visibile_style = {"visibility": "visible"}

# Register the callbacks for this graph
def register_callbacks(app):
    @app.callback(
        [Output('age_quantile_fig_placeholder', 'children'),
         Output('age_quantile_hist_title', 'children'),
         Output('age_quantile_histogram_fig_configure', 'style'),
         Output('age_quantile_histogram_col_configure', 'style')],
        [Input('age_quantile_histogram_dropdown', 'value'),
         Input('age_quantile_histogram_fig_configure', 'value'),
         Input('age_quantile_histogram_col_configure', 'value'),
         Input('non-icu-occ', 'n_clicks'),
         Input('semi-icu-occ', 'n_clicks'),
         Input('icu-occ', 'n_clicks')])
    def build_fig(dropdown_select, configuration, selected_color, value_non_icu, value_semi_icu, value_icu):
        ctx = dash.callback_context
        triggers = ctx.triggered[0]['prop_id']

        # Figuring out what/if a button was pressed
        button_pressed = None
        if triggers == 'non-icu-occ.n_clicks':
            button_pressed = 'Regular Ward'
        elif triggers == 'semi-icu-occ.n_clicks':
            button_pressed = 'semi-intensive unit'
        elif triggers == 'icu-occ.n_clicks':
            button_pressed = 'Intensive Care Ward'

        # Plotting of graphs
        if button_pressed is not None:
            return dcc.Graph(id='age_quantile_histogram_fig', figure=age_circle(selected_color,
                                                                                ward_type=button_pressed)), "Age Histogram", hidden_style, visibile_style
        else:
            if dropdown_select == 'age-quant':
                return dcc.Graph(id='age_quantile_histogram_fig', figure=age_quantile_hist_fig(
                    selected_color)), "Age Histogram", hidden_style, visibile_style
            elif dropdown_select == 'age-quant-hosp':
                if configuration == "normal":
                    graph = dcc.Graph(id='age_quantile_histogram_fig',
                                      figure=age_quantile_hosp_fig(configuration))
                elif configuration == "SIC":
                    graph = dcc.Graph(id='age_quantile_histogram_fig',
                                      figure=age_quantile_hosp_fig(configuration))
                else:
                    graph = dcc.Graph(id='age_quantile_histogram_fig',
                                      figure=age_quantile_hosp_fig(configuration))
                return html.Div(children=[
                    graph,
                    subtitle('Negative = not part of configured total')
                ]), "Age Quantile Hospitalizations", visibile_style, hidden_style
            else:
                return dcc.Graph(id='age_quantile_histogram_fig',
                                 figure=hema_dist_fig()), "Hematocrit Level Histogram", hidden_style, hidden_style

# Render the graph card
def age_quantile_histogram():
    return html.Div(children=[
        # Select the graph to show
        html.Div(className='is-flex is-justify-content-space-between', children=[
            title('Age Quantile Distribution', id='age_quantile_hist_title'),
            html.Div(style={'width': '40%'}, children=dcc.Dropdown(
                id='age_quantile_histogram_dropdown',
                options=[
                    {'label': 'Age Quantiles', 'value': 'age-quant'},
                    {'label': 'Hematocrit', 'value': 'hematocrit'},
                    {'label': 'Hopitalization by Age Quantile',
                     'value': 'age-quant-hosp'}
                ],
                value='age-quant'
            )
                     )
        ]),
        # Wrapper for the figure
        html.Div(id='age_quantile_fig_placeholder'),
        # These dropdowns are rendered conditionally to only show for their relevant graphs
        dcc.Dropdown(
            id='age_quantile_histogram_fig_configure',
            options=[
                {'label': "Compare total bed occupation", 'value': 'normal'},
                {'label': 'Compare total (Semi-)IC occupation', 'value': 'SIC'},
                {'label': 'Compare IC occupation', 'value': 'IC'},
            ],
            value='normal'
        ),
        dcc.Dropdown(
            id='age_quantile_histogram_col_configure',
            options=[
                {'label': "Red", 'value': 'indianred'},
                {'label': 'Blue', 'value': 'blue'},
                {'label': 'Green', 'value': 'green'},
            ],
            value='indianred'
        ),
    ])
