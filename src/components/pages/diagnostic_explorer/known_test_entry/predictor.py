
#importing neccessary libraries
from sklearn.linear_model import LogisticRegression
import pandas as pd 
import numpy as np

from data_processing.main_dataset import get_main_dataset

def classifier(row):
    #creates new column for classification task merging the three hospital wards
    if row['Regular Ward'] == 1:
        val = 1
    elif row['semi-intensive unit'] == 1:
        val = 2
    elif row['Intensive Care Ward'] == 1:
        val = 3
    else:
        val = 0
    return val

def prepare_data(df):
    #prepares the dataframe for training, returns X and y 
    new_data = df.fillna(0)
    new_data['Ward type']= new_data.apply(classifier, axis=1)
    new_data['SARS-Cov-2 exam result'].replace({'negative':0, 'positive':1}, inplace=True)
    X_train = new_data.drop(columns=['SARS-Cov-2 exam result', 'Ward type', 'Patient ID', 'Regular Ward', 'semi-intensive unit', 'Intensive Care Ward'])
    y_train = new_data['SARS-Cov-2 exam result']
    cols = X_train.columns[X_train.dtypes == 'object']
    X_train.drop(columns=cols, inplace=True)

    return X_train, y_train

#load the dataset
data = get_main_dataset()

#preprocess and split the data
X_train, y_train = prepare_data(data)

#train a logistic regression to export feature importances
logreg = LogisticRegression()
logreg.fit(X_train, y_train)

#export coefficients
coefs = logreg.coef_[0]
df_coefs = pd.DataFrame({'attribute':X_train.columns, 'coefficient':coefs})

#creating a dictionary to store attribute indices
attr_index = {}
predicting_attributes = df_coefs['attribute'].values
i=0
for attr in predicting_attributes:
    attr_index[attr] = i
    i += 1

def predict(attr_list):
    #formats the entered attributes so that they can be used for prediction
    #then gives the probability of covid positive
    attr_names = [item[0] for item in attr_list] #name of attributes present
    attr_values = [item[1] for item in attr_list] #values of the given attributes
    indices = [attr_index[item] for item in attr_names] #indices of present attr
    values = np.zeros(71,)
    values[indices] = attr_values
    proba_cov_positive = logreg.predict_proba(values.reshape(1,-1))[0][1]
    return proba_cov_positive

def return_attr_importances():
    #returns the feature importances extracted from the logistic regression
    df_coefs = pd.DataFrame({'attribute':X_train.columns, 'coefficient':coefs})
    df_coefs.sort_values('coefficient', ascending=False, inplace = True)
    df_coefs['coefficient'] = abs(df_coefs['coefficient'].values)
    return df_coefs