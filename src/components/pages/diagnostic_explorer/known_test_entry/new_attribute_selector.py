import dash_html_components as html
import dash_core_components as dcc

from .attribute_field import attribute_field_card
from .entropy import numerical_input_columns
from data_processing.main_dataset import get_main_dataset


data = get_main_dataset()

# Render dropdown to select a new attribute field
def new_attribute_selector(used_cols):
    # Generate new attribute list, filter out fields that are already in use
    options = [
        {'label' : item, 'value': item} for item in numerical_input_columns if item not in used_cols
    ]

    return attribute_field_card(name='Select new attribute to add', value='invalid', type='dropdown', options=options, className='has-background-danger-light')
