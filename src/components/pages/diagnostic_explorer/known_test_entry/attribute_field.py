import dash_html_components as html
import dash_core_components as dcc

# Render one attribute that can be filled in or is already filled in
# There are multiple attribute field types defined
# Although the concept stays the same, each type is rendered differently
def attribute_field_card(name, value, type='single-input-number', options=None, className=None):
    content = None

    # Encode attribute identifier in the HTML id
    id = {'type':'attribute-field', 'index': name}

    # Append the optional class
    baseClassName = 'card attribute-field'
    className =  baseClassName + ' ' + className if className is not None else baseClassName

    # Some fields cannot be removed
    removable = False

    # Render differently based on attribute field type
    if type == 'single-input-number':
        content = dcc.Input(id=id, type='number', value=value)
        removable = True
    elif type == 'text':
        content = value
        removable = True
    elif type == 'dropdown':
        content = html.Div(id=id, style={'width': '20rem'}, children=dcc.Dropdown(
            options=options,
            value=value,
            id = 'new-attr-dropdown'
        ))
    else:
        removable = True
        content = value

    # Build card header with optional delete button
    header_children = [
        html.P(className='card-header-title', children=name),
    ]
    if removable:
        header_children.append(html.Button('Delete', className='button is-white is-small', id={'type': 'atrribute-field-delete', 'index': name}))

    # Render full card by assembling header & field
    return html.Div(className='card attribute-field ' + className, style={'overflow': 'visible'}, children=[
        html.Header(className='card-header is-flex is-justify-content-space-between is-align-items-center', children=header_children),
        html.Div(className='card-content',
                 children=html.Div(className='content', children=content))
    ])
