import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Input, Output, ALL
import plotly.graph_objects as go
import pandas as pd
from urllib.parse import unquote

from components.shared.title import title, subtitle
from data_processing.main_dataset import get_main_dataset

from .attribute_field import attribute_field_card
from .new_attribute_selector import new_attribute_selector

from .predictor import predict
from .entropy import tree_from_known_values, df_scaled_input, inverse_scale, numerical_input_columns

data = get_main_dataset()

# Here we can define pre-set known values
# You can comment them out to have no pre-sets
attributes = [
    # ['Hematocrit', 0.236515447, 'single-input-number'],
    # ['Mean platelet volume ', 0.01067657, 'single-input-number'],
]

# Recursively render the decision tree
def render_decision_tree_node(node_id, tree, column_names, selected_attribute):
    left_node_id = tree.children_left[node_id]
    right_node_id = tree.children_right[node_id]
    is_split_node = left_node_id != right_node_id

    # Render depends on if this node is a leaf node or a splitting node
    if is_split_node:
        # Render children
        left_render = render_decision_tree_node(left_node_id, tree, column_names, selected_attribute)
        right_render = render_decision_tree_node(right_node_id, tree, column_names, selected_attribute)

        # Extract values for this node
        feature_name = column_names[tree.feature[node_id]]
        scaled_threshold = tree.threshold[node_id]
        threshold = inverse_scale(scaled_threshold, feature_name)

        # Encode distribution explorer data to URL data
        hash_data = feature_name + '@' + str(threshold)
        selected = hash_data == selected_attribute

        # Render node using children renders
        # Setting the id of each node to make node selection possible
        return [
            # Render left nodes
            html.Div(children=[
                html.A('{} <= {:.2f}'.format(feature_name, threshold), href='#' + hash_data, className='has-text-primary' if selected else ''),
                html.Div(children=left_render, style={'paddingLeft': '20px'})
            ]),
            # Render right nodes
            html.Div(children=[
                html.A('{} > {:.2f}'.format(feature_name, threshold), href='#' + hash_data, className='has-text-primary' if selected else ''),
                html.Div(children=right_render, style={'paddingLeft': '20px'})
            ])
        ]
    else:
        # Render a leaf node
        values = tree.value[node_id][0]
        node_class = 'negative' if values[0] > values[1] else 'positive'
        return 'Most likely outcome: ' + node_class


def register_callbacks(app):
    @app.callback(
        [
            Output('known-test-id', 'children'),
            Output('recommended-tests-tree', 'children')
        ],
        [
            Input({'type': 'attribute-field', 'index': ALL}, 'value'),
            Input('url', 'hash'),
        ]
    )
    def handle_value_changes(values, attribute):
        # Update values
        for i, value in enumerate(values):
            if i < len(values) - 1:
                attributes[i][1] = value

        # Parse the hash value from the URL
        # This may hold the selected attribute
        attribute = unquote(attribute)
        if attribute.startswith('#'):
            attribute = attribute[1:]

        # generate decision tree
        known_values = {attr[0]: attr[1] for attr in attributes}

        # Only render the decsion tree when at least one known values is set
        if len(known_values) == 0:
            rendered_nodes = html.P('Use the attribute selector to fill in your known values')
        else:
            tree = tree_from_known_values(known_values)
            rendered_nodes = render_decision_tree_node(0, tree.tree_, df_scaled_input.columns, attribute)

        return 'Known test results ({})'.format(len(values) - 1), rendered_nodes


# Render the bar in which the user can fill in known test results
def known_test_entry():
    return html.Div(children=[
        title('Known tests', id='known-test-id'),
        html.Div(className='is-flex is-flex-wrap-wrap is-align-content-center', id='entered-attributes', children=[
            # First render all attribute fields
            *[attribute_field_card(field[0], field[1], field[2])
              for field in attributes],
        ]),
        html.Div(className='is-flex is-flex-wrap-wrap is-align-content-center', id='new-attribute', children=[
            # Render the new-attribute selector as a special kind of attribute selector
            new_attribute_selector([attr[0] for attr in attributes])
        ])
    ])


def add_new_attr_callback(app):
    # adds/deletes a new attribute callback
    @app.callback(
        [
            Output('entered-attributes', 'children'),
            Output('new-attribute', 'children')
        ],
        [
            Input('new-attr-dropdown', 'value'),
            Input({'type': 'atrribute-field-delete', 'index': ALL}, 'n_clicks'),
        ])
    def add_attribute(new_colname, delete_click_cols):
        # Delete attribute if a delete click triggered this function
        # This happens when the sum of the delete_click_cols == 1 (counting None as 0)
        if sum([x if x is not None else 0 for x in delete_click_cols]) == 1:
            delete_index = delete_click_cols.index(1)
            del attributes[delete_index]

        attribute = [new_colname, 0.0, 'single-input-number']
        if new_colname not in 'invalid':
            attributes.append(attribute)

        return [attribute_field_card(field[0], field[1], field[2]) for field in attributes], new_attribute_selector([attr[0] for attr in attributes])


def recommended_tests():
    return html.Div(id='recommended-tests-tree')
