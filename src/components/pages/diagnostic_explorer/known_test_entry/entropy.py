# Learn a decision tree from weighted samples
# The samples are weighted based on their euclidian distance from the new sample

from data_processing.main_dataset import get_main_dataset

from sklearn.preprocessing import MinMaxScaler
from sklearn.tree import DecisionTreeClassifier
import math
import pandas as pd

# Use the globally available dataset as a source
df_data = get_main_dataset()

# Only select the numerical values
df_input = df_data[df_data.columns[6:]].select_dtypes(include=['float64'])
numerical_input_columns = df_input.columns

# Impute each column with mean & remove columns that are totally empty
# We do this manually because we need to known which columns are completely empty
means = df_input.mean()
df_input.fillna(means, inplace=True)
df_scaled_input = df_input.loc[:, ~df_input.isna().any()]

# Scale all numerical values between 0 and 1
scaler = MinMaxScaler()
scaler.fit(df_scaled_input)
df_scaled_input = pd.DataFrame(scaler.transform(df_scaled_input), columns = df_scaled_input.columns)

# returns a value between 0 and 1
def calc_distance(x, y):
    if math.isnan(x) or math.isnan(y):
        return 1
    else:
        return abs(x-y)

# Calculate euclidiance distance between known values and sample
# We use this distance as a sample weight in the decision tree generation step
def calc_sample_distance(known_values, sample):
    # Calculate eclidian distance between sample and known values
    distances = [calc_distance(known_values[column], sample[column]) for column in known_values.keys()]
    distance = math.sqrt(sum([d ** 2 for d in distances]))

    # Closer samples are weighted more
    return distance

# Scale the new sample
def scale_known_values(known_values):
    row = {column: [known_values[column]] if column in known_values else [0] for column in df_scaled_input.columns}
    df_known_values = pd.DataFrame.from_dict(row)
    df_scaled = pd.DataFrame(scaler.transform(df_known_values), columns = df_scaled_input.columns)

    return {column: df_scaled.loc[0, column] for column in known_values.keys()}

# Revert the scaling for displaying
def inverse_scale(value, column_name):
    row = {column: [value] if column is column_name else [0] for column in df_scaled_input.columns}
    df_value = pd.DataFrame.from_dict(row)
    return pd.DataFrame(scaler.inverse_transform(df_value), columns=df_scaled_input.columns).loc[0, column_name]

# Build the actual decision tree with the covid-outcome as a classification target
def build_decision_tree(sample_weights, excluded_columns):
    # Select input data
    input_data = df_scaled_input.loc[:, [column not in excluded_columns for column in df_scaled_input.columns]]

    # Build tree
    tree = DecisionTreeClassifier(max_depth=3, random_state=1)
    tree.fit(input_data, df_data['SARS-Cov-2 exam result'], sample_weight=sample_weights)
    return tree

# Wrapper around the decison tree builder that handles the scaling and weighting
def tree_from_known_values(known_values):
    # Scale known values on the same range as the input was scaled
    scaled_knowns = scale_known_values(known_values)

    # Calculate weight for each sample
    sample_distance = pd.Series(df_scaled_input.apply(lambda row: calc_sample_distance(scaled_knowns, row), axis=1))
    sample_weights = sample_distance.max() - sample_distance

    return build_decision_tree(sample_weights, list(known_values.keys()))