import dash_html_components as html
import dash_core_components as dcc

from components.shared.title import title, subtitle

from .known_test_entry.known_test_entry import known_test_entry, recommended_tests, add_new_attr_callback
from .known_test_entry.known_test_entry import register_callbacks as register_known_tests_callbacks

from .boxplots import boxplots, change_boxplot

# The top level component of the diagnostic explorer page (at /explore)
class DiagnosticExplorerPage:
    def __init__(self, app):
        register_known_tests_callbacks(app)
        add_new_attr_callback(app)
        change_boxplot(app)
    def render(self):
        return html.Div(className='container', children=[
            html.Div(className='tile is-ancestor is-vertical', children=[
                # Attribute fields
                html.Div(className='tile is-12 is-parent', children=[
                    html.Div(className='tile is-child',
                             children=known_test_entry())
                ]),
                html.Div(className='tile', children=[
                    # Decision tree
                    html.Div(className='tile is-5 is-parent', children=[
                        html.Div(className='tile is-child box', children=[
                            title("Recommended diagnosis paths"),
                            subtitle("Click on of the nodes in the tree to update the view on the right"),
                            recommended_tests()
                        ])
                    ]),
                    # Configurable box plots
                    html.Div(className='tile is-7 is-parent', children=[
                        html.Div(className='tile is-child box', children=[
                            boxplots()
                        ])
                    ])
                ])
            ])
        ])
