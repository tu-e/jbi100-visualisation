from data_processing.main_dataset import get_main_dataset
import dash
import dash_html_components as html
import dash_core_components as dcc
import plotly.express as px
from components.shared.title import title, subtitle
from dash.dependencies import Input, Output, ALL
from urllib.parse import unquote

# Use the globally loaded dataset as a data source
data = get_main_dataset()

# Give name to each type of ward
def classifier(row):
    # creates new column for classification task
    if row['Regular Ward'] == 1:
        val = "Regular Ward"
    elif row['semi-intensive unit'] == 1:
        val = "Semi-intensive unit"
    elif row['Intensive Care Ward'] == 1:
        val = "Intensive care unit"
    else:
        val = "No hospital ward"
    return val

# Inject column that describes the ward-type
data["Hospital Ward"] = data.apply(classifier, axis=1)

# Render the boxplots
def boxplots():
    return html.Div(id="boxplots-div", children=[
        title("Distribution explorer", id='boxplot-title'),
        # Configure boxplot
        dcc.Checklist(
            options=[
                {'label': ' Covid-19 test result', 'value': 'covid-test'},
                {'label': ' Hospital Ward', 'value': 'hospital-ward'},
                {'label': ' Include all data points ', 'value': 'all-points'}
            ],
            value=['covid-test', 'hospital-ward', 'all-points'],
            labelStyle={'display': 'inline-block'},
            id="checklist-boxplot"
        ),
        # Placeholder in which the boxplot will be rendered by a callback
        html.Div(id='boxplot-figure-wrapper'),
    ])

# Register callbacks
def change_boxplot(app):
    @app.callback(
        [
            Output('boxplot-figure-wrapper', 'children'), # Plot wrapper
            Output('boxplot-title', 'children') # Change the title depinding on the selected attribute
        ],
        [
            Input('checklist-boxplot', 'value'), # Plot configuration
            Input('url', 'hash'), # Use the hash from the URL as a data channel between the decision tree and this box plot
        ]
    )
    def update_boxplot(boxes_checked, attribute):
        # Parse the hash value from the URL
        attribute = unquote(attribute)
        if attribute.startswith('#'):
            attribute = attribute[1:]

        # Only render the boxplot if an attribute is selected
        if len(attribute) > 0:
            # Parse input to attribute and its threshold
            attribute, threshold = attribute.split('@')
            col = None
            y = None
            p = None

            # Set axis based on configuration
            if 'covid-test' in boxes_checked:
                y = "SARS-Cov-2 exam result"
            if 'hospital-ward' in boxes_checked:
                col = "Hospital Ward"
            if 'all-points' in boxes_checked:
                p = "all"
            
            # Render boxplot
            fig = px.box(data, y=y, x=attribute, color=col, points=p)

            # Add threshold as a vertical line
            fig.add_vline(x=threshold)

            # Render figure with graph wrapper
            return dcc.Graph(figure = fig, style={"height": "100%", 'width': '100%'}), "Distribution of " + attribute
        else:
            # Render a helper to direct the user
            return html.P('Select an attribute from the "Recommended diagnosis paths" graph'), "Distribution explorer"
