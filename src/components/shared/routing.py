import dash
import dash_core_components as dcc
import dash_html_components as html

from components.shared.title import title

from components.pages.hospital_overview.page import HospitalOverviewPage
from components.pages.diagnostic_explorer.page import DiagnosticExplorerPage


# This page is rendered when page is not found
def not_found():
    return title('Woeps, page not found: 404-ish')

routes = None

# Define the routes
def init_routes(app):
    return {
        '/': HospitalOverviewPage(app),
        '/explore': DiagnosticExplorerPage(app),
    }


def register_callbacks(app):
    # Routing causes all sort of "retarted plotly dash" problems
    # So we just disable the whole f*cking exception reporting
    app.config['suppress_callback_exceptions'] = True

    # Initialize routes (idempotently)
    global routes
    if routes is None:
        routes = init_routes(app)

    # Register callback to handle navigation
    @app.callback(dash.dependencies.Output('router-place', 'children'),
                  [dash.dependencies.Input('url', 'pathname')])
    def handle_url_change(pathname):
        # Try to match route
        for path, page in routes.items():
            # If the route matches, render page
            if pathname == path:
                return page.render()

        # When no route matches, render 404
        return not_found()


# Just render a placeholder to wrap the actual in page
def router(app):
    return html.Div(id='router-place')
