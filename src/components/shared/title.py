import dash_html_components as html

# Small helpers components to display titles & subtitles
def title(text, id=None):
  if id is None:
    return html.H1(text, className='title')
  else:
    return html.H1(text, id=id, className='title')

def subtitle(text):
  return html.H1(text, className='subtitle')