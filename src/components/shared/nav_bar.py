import dash_html_components as html
import dash_core_components as dcc

# Shared component that renders the top-navigation bar
def nav_bar():
    return html.Div(children=[
        html.Nav(className='navbar is-danger', role='navigation', children=[
            html.Div(className='navbar-brand', children=[
                # Main title
                html.H1(className='navbar-item', children='Spyke')
            ]),
            html.Div(className='navbar-menu', children=[
                html.Div(className='navbar-start', children=[
                    # Pages
                    dcc.Link('Hospital overview', href='/', className='navbar-item'),
                    dcc.Link('Diagnostic Explorer', href='/explore', className='navbar-item')
                ])
            ])
        ]),
        html.Div(style={'width': '100%', 'height': '2rem'})
    ])
