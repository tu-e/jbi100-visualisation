import dash_html_components as html
import dash_core_components as dcc

from data_processing.main_dataset import get_main_dataset

# Shared component that renders the circular progress bar with a given color
def circular_progressbar(id, name, progress, color='#FFEA82'):
    if id is None:
        raise Exception('id cannot be none for circular progressbar')

    return html.Div(id=id, className='circular-progressbar', n_clicks = 0, **{'data-value': progress, 'data-color': color})

# Calculate the ratio for displaying the reserved ward capacity
def return_ic_ratio(level):
    level_dict = {'non-icu':'Regular Ward', 'semi-icu':'semi-intensive unit', 'icu':'Intensive Care Ward'}

    data = get_main_dataset()
    val = len(data[data[level_dict[level]] == 1]['Patient ID'])
    n = len(data[data['SARS-Cov-2 exam result'] == 'positive']['Patient ID'])
    progress = val/n
    
    return progress