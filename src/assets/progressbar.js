/**
 * Custom JS code to animate the circular progress bars
 * This code connects the Python code to the progressbar.js library
 */

function applyProgressbar(node) {
  var bar = new ProgressBar.Circle(node, {
    strokeWidth: 6,
    easing: "easeInOut",
    duration: 1400,
    color: node.getAttribute('data-color'),
    trailColor: "#eee",
    trailWidth: 1,
    svgStyle: null,
  });

  bar.animate(node.getAttribute("data-value"));

  var observer = new MutationObserver(function (mutations) {
    mutations.forEach(function (mutation) {
      if (mutation.type == "attributes") {
        bar.animate(node.getAttribute("data-value"));
      }
    });
  });

  observer.observe(node, { attributes: true });
}

setInterval(() => {
  const found = document.getElementsByClassName("circular-progressbar");

  // Find new nodes
  for (var i = 0; i < found.length; i++) {
    const node = found[i];

    if (!node.getAttribute('added')) {
      node.setAttribute('added', true);
      applyProgressbar(node);
    }
  }
}, 1000);
