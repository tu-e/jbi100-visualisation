import pandas as pd

pd_dataset = None

# We use this central function to access the dataset so the entire dataset is only loaded once into memory
# This really reduces load/switch times
def get_main_dataset():
  global pd_dataset

  if pd_dataset is None:
    pd_dataset = pd.read_csv('data/dataset.csv', delimiter=';', decimal=',')
    pd_dataset = pd_dataset.rename(columns={'Patient addmited to regular ward (1=yes, 0=no)': 'Regular Ward', "Patient addmited to semi-intensive unit (1=yes, 0=no)": "semi-intensive unit", 'Patient addmited to intensive care unit (1=yes, 0=no)': 'Intensive Care Ward'})
  return pd_dataset


