# We structured our project as a ReactJS project: multiple pages composed as components


# Import Dash
import dash
import dash_core_components as dcc
import dash_html_components as html

# Import components
from components.shared.nav_bar import nav_bar
from components.shared.routing import router
from components.shared.routing import register_callbacks as register_router_callbacks

# Include our CSS files
# We used Bulma as our CSS framework reducing the amount of CSS we hard to write
external_css = [
    'https://codepen.io/chriddyp/pen/bWLwgP.css',
    'https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css',
    'assets/background-image.css' # The rotating Corona icon
]

# Create the Dash app
app = dash.Dash(__name__,
                external_stylesheets=external_css)

# Register callbacks from the router that allows for navigation between pages
register_router_callbacks(app)

# Top level app
app.layout = html.Div(className='top_layer', children=[
    # Provides the URL information to the router callback
    dcc.Location(id='url', refresh=False),

    # Top navigation
    nav_bar(),

    # Main router
    router(app),

    # Corona icon
    html.Img(src='/assets/Dark_viral_vect.svg', className="viral_vect"),
])

# We overwrite the base HTML template from Dash to injection Font Awesome icons & fix scaling issues
app.index_string = '''
<!DOCTYPE html>
<html>
    <head>
        {%metas%}
        <title>{%title%}</title>
        {%favicon%}
        {%css%}
        <script defer src="https://use.fontawesome.com/releases/v5.14.0/js/all.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        {%app_entry%}
        <footer>
            {%config%}
            {%scripts%}
            {%renderer%}
        </footer>
    </body>
</html>
'''

# Run server
if __name__ == '__main__':
    #app.server.run(port=8000, host='127.0.0.1', debug=True)
    app.run_server(port=8000, host='127.0.0.1', debug=True, threaded=True)
