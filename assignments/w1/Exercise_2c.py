import pandas as pd
import numpy as np

def calculate_intervals(quant):
    """calculate an interval of 5 starting at quant"""
    low = quant*5
    high = low + 5
    interval = "{} - {} years old".format(low, high)
    return interval

def get_freq(dataset, column):
    """Transforms a dataset to count the frequency of the column parameter"""
    freq = dataset.groupby([column]).count()
    sorted_freq = freq[["Patient ID"]].sort_values(by="Patient ID", ascending=False)
    sorted_freq.rename(columns={"Patient ID":"Frequency"}, inplace = True)
    sorted_freq.reset_index(inplace=True)
    return sorted_freq

def generate_quantiles(dataset, column):
    """Generates a list of the intervals of the column in the frequencies dataframe"""
    quantiles = np.array(dataset[column])
    intervals = [calculate_intervals(item) for item in quantiles]
    return intervals

data = pd.read_csv("data/dataset.csv", encoding="ISO 8859-1")
frequencies_sorted = get_freq(data, "Patient age quantile")
frequencies_sorted["Interval"] = generate_quantiles(frequencies_sorted, "Patient age quantile")

frequencies_sorted.index = np.arange(1,21) #sets index starting from 1
frequencies_sorted[:10].to_csv("data/Exercise 2c.csv") #exports into csv

