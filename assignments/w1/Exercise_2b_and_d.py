import pandas as pd

data = pd.read_csv("data/dataset.csv", encoding="ISO 8859-1")


def parse_position(column, row, df):
    """Takes an string value for column name, an int for the row and the data frame, gives out the corresponding
    value """
    return df[column][row]


def count_na_col(column, df):
    """Takes string value for column name and data frame, gives a the number of na values in column"""
    return df[column].isna().sum()


def count_na_row(row, df):
    """Takes row int and data frame, gives out the number of na values in row"""
    return df.iloc[row].isna().sum()


def count_na_all_col(df):
    """Takes data frame, gives out the number of na values in all the columns"""
    return df.isnull().sum()


def count_na_all_row(df):
    """Takes data frame, gives out the number of na values in all the rows"""
    for i in range(len(df.index)):
        print('amount na in {}: {}'.format(i, df.iloc[i].isnull().sum()))


parse_position('Hematocrit', 12, data)

count_na_all_col(data)
count_na_all_row(data)

count_na_col('Hematocrit', data)
count_na_row(12, data)
